<a name="0.0.4"></a>
## 0.0.4 (2021-02-06)


#### Features

* **RTP:**  Configure RTP by dimension ([74afe776](https://gitlab.com/melonplanet/melon-core/commit/74afe77620698b516275a89deb18c4e9859e1a5e), closes [#10](https://gitlab.com/melonplanet/melon-core/issues/10))
* **Teleport:**  Implement max homes per world ([a3a85af9](https://gitlab.com/melonplanet/melon-core/commit/a3a85af9581cb15b4317e2b8263dd4d43d5cd935))

#### Bug Fixes

* **MOTD:**  Unsquelch join message ([5b87ded2](https://gitlab.com/melonplanet/melon-core/commit/5b87ded2ab111a50a8c7134b8ff6ae7131dd46ed))
* **RTP:**
  *  Don't cast players into the void ([1795c4ed](https://gitlab.com/melonplanet/melon-core/commit/1795c4edbdf2cacb4b022178adaed186c10e17c7))
  *  Convert from real coords to chunk coords ([c31fb104](https://gitlab.com/melonplanet/melon-core/commit/c31fb10470adf89823cb6b817a0d16283c779196))



<a name="0.0.3"></a>
## 0.0.3 (2021-02-06)


#### Features

*   Implement MelonCommand class ([2d15567c](https://gitlab.com/melonplanet/melon-core/commit/2d15567cafd134b8300b690c71a2a50b7319a40a))
*   Add source command ([c1888bde](https://gitlab.com/melonplanet/melon-core/commit/c1888bde405cf7a00aa24496ddaddb73aa0136c1))
*   Add colorize utility function ([4ca56c6d](https://gitlab.com/melonplanet/melon-core/commit/4ca56c6d5a75125e290c5f747767d546a2eb0654))
*   Create paginate utility function ([8a76700f](https://gitlab.com/melonplanet/melon-core/commit/8a76700f8c72f47a77a3779e79207f9128a46e40))
* **Chat:**  Add color and prefix support ([8e1d2c5f](https://gitlab.com/melonplanet/melon-core/commit/8e1d2c5ff6a46df55ebc2e711fed7f670208ab49))
* **RTP:**
  *  Teleport player to middle of block ([dbe37937](https://gitlab.com/melonplanet/melon-core/commit/dbe37937ce4e3d0065a64cfb246cd69d89e151ff), closes [#4](https://gitlab.com/melonplanet/melon-core/issues/4))
  *  Prevent player from teleporting on top of a tree ([87fe0e0c](https://gitlab.com/melonplanet/melon-core/commit/87fe0e0c149e2181a2c24473de5534ba0a11e050))
  *  Randomize location in chunk ([9711cee2](https://gitlab.com/melonplanet/melon-core/commit/9711cee27b5448eb897b7cdffc00387ee2762014))
  *  Scan entire selected chunk for a safe place to go ([86617425](https://gitlab.com/melonplanet/melon-core/commit/86617425cbf1f84b87a752adff67135e44831458))
  *  Report information to user ([fc832c21](https://gitlab.com/melonplanet/melon-core/commit/fc832c21bae8b7fa772d01bf18f8dbf91910436c))
  *  Initial implementation of RTP ([6ca54c3e](https://gitlab.com/melonplanet/melon-core/commit/6ca54c3e4a890a66a469ef1387d1b9089bf197ba))
* **Rules:**  Create rules module ([9a7943a1](https://gitlab.com/melonplanet/melon-core/commit/9a7943a14d0fdc42f84ab3881bec8a50623b2cf4))
* **Teleport:**
  *  Implement basic TPA ([3a817f9c](https://gitlab.com/melonplanet/melon-core/commit/3a817f9c14857f8d8997d9cc046819f2f4558127), closes [#6](https://gitlab.com/melonplanet/melon-core/issues/6))
  *  Implement homes ([fd745319](https://gitlab.com/melonplanet/melon-core/commit/fd7453197b94204f6f8078bd40af1431e255f66c), closes [#7](https://gitlab.com/melonplanet/melon-core/issues/7))

#### Bug Fixes

* **Hammer:**  Show AM/PM and timezone in action messages ([3d291ecc](https://gitlab.com/melonplanet/melon-core/commit/3d291ecc9c01f825e760847d2addfccaf2da3a6b))
* **MOTD:**  Format motd when sent via command ([cbd5599d](https://gitlab.com/melonplanet/melon-core/commit/cbd5599d2cf46f2e56d666f9d5061d5bd49e5b11))



<a name="0.0.2"></a>
## 0.0.2 (2021-01-27)


#### Features

*   broadcastToPermission utility function ([a210ee72](https://gitlab.com/melonplanet/melon-core/commit/a210ee7249c195c4cdb36c1ce21ce850e87e5570))
*   List players associated with IP on join ([9c88f35d](https://gitlab.com/melonplanet/melon-core/commit/9c88f35da4aeb45764f69733ea26d5534d2967af))
*   Hammer now tracks IPs ([84968a0e](https://gitlab.com/melonplanet/melon-core/commit/84968a0e1d9b9b66c38231d70243d081a258c3a9))
*   Hammer now tracks name changes ([d22432f4](https://gitlab.com/melonplanet/melon-core/commit/d22432f42789dea62a2cb3ce4d21698becf0c545))
*   Create Hammer module ([b01f01ab](https://gitlab.com/melonplanet/melon-core/commit/b01f01ab4bfdd66be86da3c4de07685a23a3f036))
*   Depend on melon-libs instead of KotlinBukkitAPI ([1e6cbf24](https://gitlab.com/melonplanet/melon-core/commit/1e6cbf24a3091b406a6e80b7a2d5abbc5ef433f0))
* **Hammer:**
  *  Add mod chat functionality ([76f99451](https://gitlab.com/melonplanet/melon-core/commit/76f99451fc60b77d1a876dbefbe95bcd77552913))
  *  Broadcast action messages to other moderators ([aa4fb0a3](https://gitlab.com/melonplanet/melon-core/commit/aa4fb0a3f5c8c786913d28befac0e29683ed7aec))
  *  Improve history formatting ([651e9333](https://gitlab.com/melonplanet/melon-core/commit/651e93337c9f888fcc2b8b8db367793a94742646))
  *  Add warn command ([11fdcb91](https://gitlab.com/melonplanet/melon-core/commit/11fdcb91e34004b6d078595cf80396711241a20d))
  *  Add unban command ([0bccef7b](https://gitlab.com/melonplanet/melon-core/commit/0bccef7b5165a50b20c6216dba5d48b2b9a36e38))
  *  Add kick command ([9b51ff99](https://gitlab.com/melonplanet/melon-core/commit/9b51ff996e0c5ab0c03194a70f370cdbb057cc44))
  *  Rename list-alts option ([45ab541d](https://gitlab.com/melonplanet/melon-core/commit/45ab541dbab20f3a22a1194c73a40c545bdb0af5))
  *  Add action records to history command ([60c4eb69](https://gitlab.com/melonplanet/melon-core/commit/60c4eb6927f80953a8d2689e394a359d220dd354))
  *  Basic implementation of bans ([70d28938](https://gitlab.com/melonplanet/melon-core/commit/70d289382165a9f86d887c9f66de4d413494fcf9))
  *  List player alts in history ([c68b03d0](https://gitlab.com/melonplanet/melon-core/commit/c68b03d01ee89603d41698c1ec470c16d0c00a66))
  *  History by player name ([65036bc3](https://gitlab.com/melonplanet/melon-core/commit/65036bc3070946231a35cdc79f7ed23683397111))
  *  History by uuid ([7eeb1120](https://gitlab.com/melonplanet/melon-core/commit/7eeb1120bb23335da0e5ead7d156411d41e2de55))



<a name="0.0.1"></a>
## 0.0.1 (2021-01-10)


#### Features

*   Squelch join message if player is receiving an MOTD ([1c4dfd8b](https://gitlab.com/melonplanet/melon-core/commit/1c4dfd8b68809fb8066e654074396c8c7eeeb2d2))
*   Add basic contemplating and color support to MOTD ([d96ab97e](https://gitlab.com/melonplanet/melon-core/commit/d96ab97eaed9baacf75c1ba2c883ba67d0caea61))
*   Change echo to ping ([dc7255ee](https://gitlab.com/melonplanet/melon-core/commit/dc7255ee234625ad64b44d29227f1444f92859e1))
*   Send Motd to player on join ([2d9316fa](https://gitlab.com/melonplanet/melon-core/commit/2d9316fa05c2a64ef80828082c1bf6eb03af3fec))
*   Dynamically load motd command ([bbbac71a](https://gitlab.com/melonplanet/melon-core/commit/bbbac71a4aa2c4f4d24e9fe77adf029fa8606847))
*   Softdepend Kotlin plugin ([a1e34803](https://gitlab.com/melonplanet/melon-core/commit/a1e34803718b8b3fb4891690c332c683b397867b))
*   Intital minimal working plugin ([7e2123c3](https://gitlab.com/melonplanet/melon-core/commit/7e2123c303c6f459ca6f8d57c4cb002b054d1a9c))



