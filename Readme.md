MelonCore
=========

The core mechanics and administration plugin for MelonPlanet

[Download latest version](https://gitlab.com/melonplanet/melon-core/-/jobs/artifacts/main/browse?job=build)

Features
========

Basic Utility commands
----------------------

### Ping

```
/ping
```

Does not have any options, simply replies to the player with "Pong"

MOTD
----

Will show the player an MOTD on join. Will only show this to a player once within the specified time interval (state is not persistent and the timer resets on server restart)

Also provides the `/motd` command to view it again.

Supports multi line MOTDs, templating, and color codes, like so:

```yaml
motd:
  message:
    - First line
    - Welcome &r<player>
    - Third line
```

Implicitly resets the formatting/color at the end of each line

### Templating

Supported values:

-	`<player>` : Fills in the player's username

Hammer
------

Provides general administration and player management functions

### Username/IP tracking

Keeps a record of all the usernames and IPs a player has joined a server via.

Logs known username/IPs for a player to the console on join, as well as the players that have been seen at a particular IP.

### Warn/Kick/Ban

Allows warning, kicking, and banning players (even offline ones) by name or uuid. Warns and bans can have a specified duration, after which they will expire. Warn, kick, and ban messages are configurable through the plugin config.yml.

### Chat

Moderator only chat through the `/hammer chat <message>` command

### Commands

#### History

`/hammer history [options] <target>`

Provides access to the history of enforcement actions for a given player.

Options:

-	`-a`, `--alts`: Adds a list of possible alts for the given player
-	`-c INT`, `--count INT`: Number of actions to show per page
-	`-p INT`, `--page INT`: Page number to show
-	`-e`, `--expired`: Include expired actions
-	`-l`, `--all`: Show all information (equivalent to `-a -e`\)
-	`-h`, `--help`: Show the help information

#### Warn

`/hammer warn [options] <target> <reason>`

Applies a warning to a player

Options:

-	`-d`, `--duration`: Duration the warning should last for (e.g. `1d5h`\)
-	`-h`, `--help`: Show the help

#### Kick

`/hammer kick <target> <reason>`

Kicks a player

Options:

-	`-h`, `--help`: Show the help

#### Ban

`/hammer ban [options] <target> <reason>`

Bans a player

Options:

-	`-d`, `--duration`: Duration to ban the player for (e.g. `1d5h`\)
-	`-h`, `--help`: Show the help

#### Unban

`/hammer unban [options] <target> <reason>`

Bans a player

Options:

-	`-h`, `--help`: Show the help

Rules
-----

Input rules into the config as a list of strings. These accept `&` based format codes, and will a newline and reset code will be automatically inserted between each line.

The `/rules` command, accessible with the `meloncore.rules` permission, will paginate and display the rules.

RTP
---

`/rtp`

Will attempt to teleport the player to a random location within the region defined in the config file. Will make a configurable number of attempts to do so, and will load chunks async as to not cause lag.

Teleport
--------

### Homes

Allows the player to have a (currently) arbitrary number of homes. `/home --help` for usage
