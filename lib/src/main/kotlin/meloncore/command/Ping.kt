package net.melonpla.core.command.ping

import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender

object PingCmd : CommandExecutor {
    override fun onCommand(
        sender: CommandSender,
        cmd: Command,
        lbl: String,
        args: Array<out String>
    ): Boolean {
        sender.sendMessage("Pong")

        return true
    }
}
