package net.melonpla.core.command.source

import net.melonpla.core.util.*
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender

object SourceCmd : CommandExecutor {
    override fun onCommand(
        sender: CommandSender,
        cmd: Command,
        lbl: String,
        args: Array<out String>
    ): Boolean {
        sender.sendMessage(
            "&a[&4MelonCore&a]&r The MelonCore plugin is licensed under the AGPL3".colorize()
        )
        sender.sendMessage(
            "&a[&4MelonCore&a]&r Source Code:&7 https://gitlab.com/melonplanet/melon-core".colorize()
        )

        return true
    }
}
