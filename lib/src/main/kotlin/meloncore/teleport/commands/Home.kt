package net.melonpla.core.teleport.home

import com.github.ajalt.clikt.core.*
import com.github.ajalt.clikt.parameters.arguments.*
import com.github.ajalt.clikt.parameters.groups.*
import com.github.ajalt.clikt.parameters.options.*
import net.melonpla.core.command.MelonCommand
import net.melonpla.core.teleport.Teleport
import net.melonpla.core.teleport.database.*
import net.melonpla.core.util.*
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.Logger

public class HomeCmd(val teleport: Teleport, log: Logger, val maxHomes: Int) :
    MelonCommand(
        "home",
        "Teleport to or manage your homes",
        "/home --(set|list|delete|help) <name>",
        "meloncore.teleport.home",
        log,
        onlyPlayer = true
    ) {
    override fun genCommand(sender: CommandSender, alias: String, args: Array<out String>): CliktCommand {
        return HomeBase(sender, teleport.db, log, maxHomes)
    }
}

class HomeBase(
    val sender: CommandSender,
    val db: Database,
    val log: Logger,
    val maxHomes: Int
) : CliktCommand(
    help = "Teleport to or manage your homes",
    name = "home"
) {
    val player = sender as Player
    val action: String by mutuallyExclusiveOptions<String>(
        option("--list", "-l", help = "List homes").flag().convert { if (it) "list" else null },
        option("--set", "-s", help = "Set home").flag().convert { if (it) "set" else null },
        option("--delete", "--del", "-d", help = "Delete home").flag().convert { if (it) "del" else null }
    ).single().default("go")
    val home: String? by argument(help = "Name of home to teleport to").optional()
    override fun run() {
        transaction(db) {
            // Attempt use default home if not available
            val h = home ?: "default"
            // Look up the player
            val p = player.lookupOrCreateTp()
            when (action) {
                "go" -> {
                    val dest = p.getHome(h)
                    if (dest == null) {
                        player.sendMessage(
                            "Home &7$h&r has not been set yet".colorize()
                        )
                    } else {
                        val world = Bukkit.getServer().getWorld(dest.world)
                        if (world == null) {
                            player.sendMessage(
                                "Home &7$h&r is in world &7${dest.world}&r, which does not exist".colorize()
                            )
                        } else {
                            val loc = Location(world, dest.x, dest.y, dest.z)
                            player.teleportAsync(loc)
                        }
                    }
                }
                "list" -> {
                    player.sendMessage("&6======== &rHomes&6 ======&r".colorize())
                    for (home in p.homes) {
                        val x = home.x.toLong()
                        val y = home.y.toLong()
                        val z = home.z.toLong()
                        player.sendMessage(
                            "  - &7Home&r: ${home.name} (World: ${home.world}) ($x,$y,$z)"
                                .colorize()
                        )
                    }
                }
                "set" -> {
                    val loc = player.getLocation()
                    if (p.setHome(h, loc, maxHomes)) {
                        player.sendMessage("Home &7$h&r set".colorize())
                    } else {
                        player.sendMessage(
                            "&4You have reached your maximum number of homes ($maxHomes) for this world.&r"
                                .colorize()
                        )
                    }
                }
                "del" -> {
                    p.delHome(h)
                    player.sendMessage("Home &7$h&r deleted".colorize())
                }
                else -> log.info("This actually shouldn't happen: $action")
            }
        }
    }
}
