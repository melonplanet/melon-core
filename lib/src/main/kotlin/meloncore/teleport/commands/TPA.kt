package net.melonpla.core.teleport.tpa

import net.melonpla.core.teleport.database.*
import net.melonpla.core.util.*
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import org.bukkit.command.defaults.BukkitCommand
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitRunnable
import org.jetbrains.exposed.sql.*
import org.slf4j.Logger
import org.stringtemplate.v4.*
import java.util.UUID

data class Request(val requestor: UUID, val time: Long)

class TPAData(
    // Timeout for requests, in milliseconds
    val timeout: Long,
    // Template for "there" message
    val thereMessage: String,
    // Template for "here" message
    val hereMessage: String,
    // Logger
    val log: Logger,
) {
    // Maps the target to the current request to teleport to them
    val there: HashMap<UUID, Request> = HashMap()

    // Maps the target to the current request to teleport them to the recipient
    val here: HashMap<UUID, Request> = HashMap()

    // Schedule clean up tasks, removing requests after the time out has expired
    fun scheduleTasks(plugin: JavaPlugin) {
        // Clean up expired requests from the requests map
        val expired = object : BukkitRunnable() {
            public override fun run() {
                val currentTime = System.currentTimeMillis()
                // sweep tpa requests
                val expiredUUIDs: ArrayList<UUID> = arrayListOf()
                for ((uuid, request) in there) {
                    if (request.time + timeout < currentTime) {
                        expiredUUIDs.add(uuid)
                    }
                }
                // Remove expired ones
                for (uuid in expiredUUIDs) {
                    val removed = there.remove(uuid)
                    log.info(
                        "Tpa request ${uuid.playerName()} -> ${removed?.requestor?.playerName()} expired"
                    )
                }
                // Clear list and sweep tpahere requests
                expiredUUIDs.clear()
                for ((uuid, request) in here) {
                    if (request.time + timeout < currentTime) {
                        expiredUUIDs.add(uuid)
                    }
                }
                // Remove expired ones
                for (uuid in expiredUUIDs) {
                    val removed = here.remove(uuid)
                    log.info(
                        "Tpahere request ${uuid.playerName()} -> ${removed?.requestor?.playerName()} expired"
                    )
                }
            }
        }
        // Run every 5 ticks/0.25s
        expired.runTaskTimerAsynchronously(plugin, 5, 5)
    }
}

public class TPACmd(val tpa: TPAData) : BukkitCommand("tpa") {
    val log = tpa.log
    init {
        this.description = "Send a request to another player to teleport to them"
        this.usageMessage = "/tpa <player>"
        this.permission = "meloncore.teleport.tpa"
    }
    override fun execute(sender: CommandSender, alias: String, args: Array<out String>): Boolean {
        if (sender !is Player) {
            sender.sendMessage("&4This command must be run by a player.&r".colorize())
            return false
        } else if (!sender.hasPermission("meloncore.teleport.tpa")) {
            sender.sendMessage("&4You do not have permission to run this command.&r".colorize())
            return false
        } else if (args.size != 1) {
            sender.sendMessage("&4${this.usageMessage}&r".colorize())
            return false
        }
        val target = Bukkit.getServer().getPlayer(args[0])
        if (target == null) {
            sender.sendMessage("&4No such player:&7${args[0]}&4 online&r".colorize())
            return false
        } else if (!target.hasPermission("meloncore.teleport.tpa")) {
            sender.sendMessage("&4Player &7${args[0]}&r does not have permission to accept requests.")
            return false
        }
        val currentTime = System.currentTimeMillis()
        val senderName = sender.getPlayerListName()
        log.info("$senderName has requested to teleport to ${target.getPlayerListName()}")
        val request = Request(sender.getUniqueId(), currentTime)
        tpa.there.put(target.getUniqueId(), request)
        val template = ST(tpa.thereMessage)
        template.add("sender", senderName)
        target.sendMessage(template.render().colorize())
        return true
    }
}

public class TPAHereCmd(val tpa: TPAData) : BukkitCommand("tpahere") {
    val log = tpa.log
    init {
        this.description = "Send a request to another player to teleport to them to you"
        this.usageMessage = "/tpahere <player>"
        this.permission = "meloncore.teleport.tpa"
    }
    override fun execute(sender: CommandSender, alias: String, args: Array<out String>): Boolean {
        if (sender !is Player) {
            sender.sendMessage("&4This command must be run by a player.&r".colorize())
            return false
        } else if (!sender.hasPermission("meloncore.teleport.tpa")) {
            sender.sendMessage("&4You do not have permission to run this command.&r".colorize())
            return false
        } else if (args.size != 1) {
            sender.sendMessage("&4${this.usageMessage}&r".colorize())
            return false
        }
        val target = Bukkit.getServer().getPlayer(args[0])
        if (target == null) {
            sender.sendMessage("&4No such player:&7${args[0]}&4 online&r".colorize())
            return false
        } else if (!target.hasPermission("meloncore.teleport.tpa")) {
            sender.sendMessage("&4Player &7${args[0]}&r does not have permission to accept requests.")
            return false
        }
        val currentTime = System.currentTimeMillis()
        val senderName = sender.getPlayerListName()
        log.info("$senderName has requested to have ${target.getPlayerListName()} teleported")
        val request = Request(sender.getUniqueId(), currentTime)
        tpa.here.put(target.getUniqueId(), request)
        val template = ST(tpa.thereMessage)
        template.add("sender", senderName)
        target.sendMessage(template.render().colorize())
        return true
    }
}

// Accept the most recent tpa request
public class TPAcceptCmd(val tpa: TPAData) : BukkitCommand("tpaccept") {
    val log = tpa.log
    init {
        this.description = "Accept a teleport request"
        this.usageMessage = "/tpaccept"
        this.permission = "meloncore.teleport.tpa"
    }
    override fun execute(sender: CommandSender, alias: String, args: Array<out String>): Boolean {
        if (sender !is Player) {
            sender.sendMessage("&4This command must be run by a player.&r".colorize())
            return false
        } else if (!sender.hasPermission("meloncore.teleport.tpa")) {
            sender.sendMessage("&4You do not have permission to run this command.&r".colorize())
            return false
        } else if (args.size != 0) {
            sender.sendMessage("&4${this.usageMessage}&r".colorize())
            return false
        }
        val inbound = tpa.there.remove(sender.getUniqueId())
        val outbound = tpa.here.remove(sender.getUniqueId())
        val (from, to) = if (inbound != null) {
            val to = sender
            val from = Bukkit.getServer().getPlayer(inbound.requestor)
            if (from == null) {
                sender.sendMessage(
                    "&4Player ${inbound.requestor.playerName()} is no longer online.&r".colorize()
                )
                return true
            } else {
                Pair(from, to)
            }
        } else if (outbound != null) {
            val from = sender
            val to = Bukkit.getServer().getPlayer(outbound.requestor)
            if (to == null) {
                sender.sendMessage(
                    "&4Player ${outbound.requestor.playerName()} is no longer online.&r".colorize()
                )
                return true
            } else {
                Pair(from, to)
            }
        } else {
            sender.sendMessage("&4No active teleport requests&r".colorize())
            return true
        }
        from.teleport(to)
        return true
    }
}
