package net.melonpla.core.teleport

import com.github.ajalt.clikt.core.*
import net.melonpla.core.teleport.database.*
import net.melonpla.core.teleport.home.HomeCmd
import net.melonpla.core.teleport.tpa.*
import net.melonpla.core.util.*
import org.bukkit.command.defaults.BukkitCommand
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.plugin.java.JavaPlugin
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.LoggerFactory

public class Teleport(val config: ConfigurationSection, val plugin: JavaPlugin) {
    val log = LoggerFactory.getLogger("MelonTP")
    val dbName = config.getString("database.h2") ?: "teleport"
    val dbPath = "jdbc:h2:${plugin.getDataFolder().getAbsolutePath()}/$dbName"
    init { log.info("Opening db connection at: $dbPath") }
    val db: Database = Database.connect(dbPath, driver = "org.h2.Driver", user = "root", password = "")
    init {
        log.info("Database connection opened")
        log.info("Creating schema")
        transaction(db) {
            SchemaUtils.create(Players, Homes)
        }
        log.info("Schema created")
    }

    fun commands(): List<BukkitCommand> {
        // Config value is in seconds, convert to millis
        val tpaTimeout = config.getLong("tpa.timeout") * 1000
        val tpaThere = config.getStringList("tpa.there-message").joinToString(separator = "\n")
        val tpaHere = config.getStringList("tpa.here-message").joinToString(separator = "\n")
        val tpa = TPAData(tpaTimeout, tpaThere, tpaHere, log)
        tpa.scheduleTasks(plugin)
        val maxHomes = config.getInt("homes.max")
        return listOf(HomeCmd(this, log, maxHomes), TPACmd(tpa), TPAHereCmd(tpa), TPAcceptCmd(tpa))
    }
}
