package net.melonpla.core.teleport.database

import org.bukkit.Location
import org.bukkit.entity.Player
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.*
import org.stringtemplate.v4.*

object Homes : IntIdTable() {
    var player = reference("player", Players)
    var name = varchar("name", 256)
    var world = varchar("world", 256)
    var x = double("x")
    var y = double("y")
    var z = double("z")
}

public class Home(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, Home>(Homes)
    var player by Homes.player
    var name by Homes.name
    var world by Homes.world
    var x: Double by Homes.x
    var y: Double by Homes.y
    var z: Double by Homes.z
}

object Players : IntIdTable() {
    var uuid = uuid("uuid")
}

public class TpPlayer(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, TpPlayer>(Players)
    var uuid by Players.uuid
    val homes by Home referrersOn Homes.player

    fun getHome(name: String): Home? {
        return this
            .homes
            .filter { it.name == name }
            .firstOrNull()
    }

    fun setHome(homeName: String, loc: Location, max: Int): Boolean {
        val home = this.homes.filter { it.name == homeName }.firstOrNull()
        if (home == null) {
            val total = this.homes.filter { it.world == loc.getWorld().getName() }.count()
            if (total >= max) {
                return false
            }
            val id = this.id
            Homes.insert {
                it[player] = id.value
                it[name] = homeName
                it[world] = loc.getWorld().getName()
                it[x] = loc.getX()
                it[y] = loc.getY()
                it[z] = loc.getZ()
            }
            return true
        } else {
            home.world = loc.getWorld().getName()
            home.x = loc.getX()
            home.y = loc.getY()
            home.z = loc.getZ()
            return true
        }
    }

    fun delHome(homeName: String) {
        val id = this.id.value
        Homes.deleteWhere() { (Homes.player eq id) and (Homes.name eq homeName) }
    }
}

public fun Player.lookupTp(): TpPlayer? {
    val uuid = this.getUniqueId()
    return TpPlayer.find { Players.uuid eq uuid }.firstOrNull()
}

public fun Player.lookupOrCreateTp(): TpPlayer {
    val tp = this.lookupTp()
    if (tp == null) {
        val u = this.getUniqueId()
        val id = Players.insertAndGetId { it[uuid] = u }
        return TpPlayer.findById(id)!!
    } else {
        return tp
    }
}
