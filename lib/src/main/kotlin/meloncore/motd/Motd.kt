package net.melonpla.core.motd

import net.melonpla.core.util.*
import org.bukkit.command.CommandSender
import org.bukkit.command.defaults.BukkitCommand
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent
import org.stringtemplate.v4.ST
import java.util.UUID

public class Motd(val config: ConfigurationSection) : BukkitCommand("motd"), Listener {
    val waitTime: Int = config.getInt("wait-time")

    // Combine into a single string with implicit formatting resets at the end of each line
    val message: String = config.getStringList("message").joinToString(separator = "&r\n")
    val lastTime: HashMap<UUID, Long> = HashMap()
    init {
        this.description = "Display the server message of the day"
        this.usageMessage = "/motd"
        this.setPermission("meloncore.motd")
    }
    override fun execute(sender: CommandSender, alias: String, args: Array<out String>): Boolean {
        if (!sender.hasPermission(this.getPermission()!!)) {
            sender.sendMessage("You do not have permission to run this command")
            return true
        }
        if (sender !is Player) {
            sender.sendMessage("This command can only be run by a player")
            return false
        }
        if (args.size > 0) {
            sender.sendMessage("This command does not take any arguments")
            return false
        }

        var m = ST(message)
        m.add("player", sender.getPlayerListName())
        sender.sendMessage(m.render().colorize())
        return true
    }

    @EventHandler
    public fun onPlayerJoin(event: PlayerJoinEvent) {
        var uuid = event.getPlayer().getUniqueId()
        var currentTime: Long = System.currentTimeMillis() / 1000
        var lastTimeVal = lastTime.put(uuid, currentTime)
        if ((lastTimeVal != null && currentTime - lastTimeVal >= waitTime) || lastTimeVal == null) {
            // setup template values
            var player = event.getPlayer()
            var m = ST(message)
            m.add("player", player.getPlayerListName())
            // Perform templating and colorization
            var colored = m.render().colorize()
            player.sendMessage(colored)
        }
    }
}
