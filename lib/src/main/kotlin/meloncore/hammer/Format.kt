package net.melonpla.core.hammer.format
import net.melonpla.core.hammer.player.ActionType
import java.time.Duration

fun formatAction(
    type: ActionType,
    recipient: String,
    giver: String,
    reason: String? = null,
    duration: Duration? = null,
    durationLess: Boolean = false
): String {
    val output = StringBuilder()
    output.append("&4[Hammer]&r Player &5")
    output.append(recipient)
    output.append("&r was ")
    output.append(
        when (type) {
            ActionType.Unban -> "&aunbanned&r"
            ActionType.Warning -> "&ewarned&r"
            ActionType.Kick -> "&6kicked&r"
            ActionType.Ban -> "&4banned&r"
        }
    )
    output.append(" by &7")
    output.append(giver)
    output.append("&r")
    if (reason != null) {
        output.append(" for: &8")
        output.append(reason)
        output.append("&r")
    }
    if (duration != null) {
        output.append(" for: &3")
        output.append(
            duration
                .toString()
                .substring(2)
                .replace("(\\d[HMS])(?!$)", "$1 ")
                .toLowerCase()
        )
        output.append("&r")
    }
    if (duration == null && !durationLess) {
        output.append(" for &3Forever&r")
    }
    return output.toString()
}
