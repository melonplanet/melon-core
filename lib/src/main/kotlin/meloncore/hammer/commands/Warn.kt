package net.melonpla.core.hammer.commands
import com.github.ajalt.clikt.core.*
import com.github.ajalt.clikt.parameters.arguments.*
import com.github.ajalt.clikt.parameters.options.*
import net.melonpla.core.hammer.format.formatAction
import net.melonpla.core.hammer.player.*
import net.melonpla.core.util.*
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

class HammerWarn(val player: CommandSender, val db: Database, val fmt: String) : CliktCommand(
    help = "Warns a player",
    name = "warn"
) {
    val playerName by argument(help = "Player name or UUID")
    val reason by argument(help = "Reason for warn").multiple()
    val warnRawDuration: String? by option("-d", "--duration", help = "Duration to warn player for")
    val permission = "meloncore.hammer.warn"
    override fun run() {
        val combinedReason = reason.joinToString(separator = " ")
        if (!player.hasPermission(permission) && !(player is ConsoleCommandSender)) {
            player.sendMessage("You do not have permission to run this command")
        } else {
            transaction(db) {
                val output = StringBuilder()
                val uuid = parseUUID(playerName)
                val playerRecord = uuid?.let { findPlayerByUUID(it) } ?: findPlayerByName(
                    playerName
                )
                if (playerRecord == null) {
                    output.appendLine("&4No Such Player Found&r")
                } else {
                    val warnDuration = warnRawDuration?.let { it.toDuration() }
                    val giver = when (player) {
                        is ConsoleCommandSender -> null
                        is Player -> findPlayerByUUID(player.getUniqueId())
                        else -> null
                    }
                    val record = playerRecord.performAction(
                        ActionType.Warning,
                        combinedReason,
                        giver,
                        warnDuration
                    )
                    output.appendLine(
                        formatAction(
                            ActionType.Warning,
                            playerRecord.mostRecentName(),
                            giver?.mostRecentName() ?: "CONSOLE",
                            combinedReason,
                            duration = warnDuration
                        )
                    )
                    Bukkit.getPlayer(playerRecord.uuid)?.sendMessage(record.format(fmt).colorize())
                }
                output.toString().colorize().broadcastToPermission(permission)
            }
        }
    }
}
