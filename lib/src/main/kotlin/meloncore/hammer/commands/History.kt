package net.melonpla.core.hammer.commands
import com.github.ajalt.clikt.core.*
import com.github.ajalt.clikt.parameters.arguments.*
import com.github.ajalt.clikt.parameters.options.*
import com.github.ajalt.clikt.parameters.types.int
import net.melonpla.core.hammer.player.*
import net.melonpla.core.util.*
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

class HammerHistory(val player: CommandSender, val db: Database) : CliktCommand(
    help = "Provides player history information",
    name = "history"
) {
    val playerName by argument(help = "Player name or uuid")
    val listAlts by option("-a", "--alts", help = "Show possible alts for player")
        .flag(default = false)
    val listActions: Int by option("-c", "--count", help = "Number of previous actions to show")
        .int()
        .default(5)
    val page: Int by option("-p", "--page", help = "Page on action history").int().default(1)
    val showExpired by option("-e", "--expired", help = "Show expired actions")
        .flag(default = false)
    val showAll by option("-l", "--all", help = "Show all information").flag(default = false)
    val permission = "meloncore.hammer.history"
    override fun run() {
        if (!player.hasPermission(permission) && !(player is ConsoleCommandSender)) {
            player.sendMessage("You do not have permission to run this command")
        } else {
            transaction(db) {
                val output = StringBuilder()
                val uuid = parseUUID(playerName)
                val playerRecord: PlayerRecord? = uuid?.let { findPlayerByUUID(uuid) } ?: findPlayerByName(
                    playerName
                )
                if (playerRecord == null) {
                    output.appendLine("&4No Such Player Found&r")
                } else {
                    output.append("&2History for:&r ")
                    // Name History
                    output.appendLine(playerRecord.mostRecentName())
                    output.append(" - &7Recent Names:&r ")
                    output.appendLine(
                        playerRecord
                            .nameRecords
                            .sortedByDescending { it.lastSeen }
                            .take(5)
                            .map { it.name }
                            .joinToString()
                    )
                    // IP History
                    output.append(" - &7Recent IPs:&r ")
                    output.appendLine(
                        playerRecord
                            .ipRecords
                            .sortedByDescending { it.lastSeen }
                            .take(5)
                            .map { it.ip }
                            .joinToString()
                    )
                    // List alts
                    if (listAlts || showAll) {
                        output.appendLine(" - &7Possible Alts:&r")
                        val alts = possibleAlts(playerRecord)
                        for (alt in alts) {
                            output.append("   - ")
                            output.appendLine(alt.mostRecentName())
                        }
                    }
                    // List history
                    if (listActions > 0) {
                        val rawActions = playerRecord
                            .actionRecords
                            .sortedByDescending { it.time }
                            .filter { showAll || showExpired || !it.expired() }
                        val total = rawActions.count()
                        val actions = rawActions
                            .drop((page - 1) * listActions)
                            .take(listActions)
                        val pages = total / listActions + 1
                        output.appendLine(
                            " - &7Action History (Page $page/$pages, $total Total Records):&r"
                        )
                        for (action in actions) {
                            val line = if (action.expired()) {
                                action.formatLog(
                                    "   - <type>: <date> <reason> &7(<giver>) &9Expired&r"
                                )
                            } else if (action.expires()) {
                                action.formatLog(
                                    "   - <type>: <date> <reason> &7(<giver>) &cExpires&r: <expires>"
                                )
                            } else {
                                action.formatLog(
                                    "   - <type>: <date> <reason> &7(<giver>)&r"
                                )
                            }
                            output.appendLine(line)
                        }
                    }
                }
                player.sendMessage(output.toString().colorize())
            }
        }
    }
}
