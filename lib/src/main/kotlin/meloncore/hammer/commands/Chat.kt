package net.melonpla.core.hammer.commands
import com.github.ajalt.clikt.core.*
import com.github.ajalt.clikt.parameters.arguments.*
import com.github.ajalt.clikt.parameters.options.*
import net.melonpla.core.hammer.player.*
import net.melonpla.core.util.*
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player
import org.jetbrains.exposed.sql.*

class HammerChat(val player: CommandSender) : CliktCommand(
    help = "Sends a message to all online moderators",
    name = "chat"
) {
    val rawMessage by argument(help = "Message to send").multiple()
    val permission = "meloncore.hammer.chat"
    override fun run() {
        val message = rawMessage.joinToString(separator = " ")
        if (!player.hasPermission(permission) && !(player is ConsoleCommandSender)) {
            player.sendMessage("You do not have permission to run this command")
        } else {
            val sender = if (player is Player) {
                player.getPlayerListName()
            } else {
                "CONSOLE"
            }
            val output = StringBuilder()
            output.append("&4[Hammer] &7<")
            output.append(sender)
            output.append(">&r ")
            output.append(message)
            output.toString().colorize().broadcastToPermission(permission)
        }
    }
}
