package net.melonpla.core.hammer.commands
import com.github.ajalt.clikt.core.*
import com.github.ajalt.clikt.parameters.arguments.*
import com.github.ajalt.clikt.parameters.options.*
import net.melonpla.core.hammer.format.formatAction
import net.melonpla.core.hammer.player.*
import net.melonpla.core.util.*
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

class HammerKick(val player: CommandSender, val db: Database, val fmt: String) : CliktCommand(
    help = "Kicks a player",
    name = "kick"
) {
    val playerName by argument(help = "Player name or UUID")
    val reason by argument(help = "Reason for ban").multiple()
    val permission = "meloncore.hammer.kick"
    override fun run() {
        val combinedReason = reason.joinToString(separator = " ")
        if (!player.hasPermission(permission) && !(player is ConsoleCommandSender)) {
            player.sendMessage("You do not have permission to run this command")
        } else {
            transaction(db) {
                val output = StringBuilder()
                val uuid = parseUUID(playerName)
                val playerRecord = uuid?.let { findPlayerByUUID(it) } ?: findPlayerByName(
                    playerName
                )
                if (playerRecord == null) {
                    output.appendLine("&4No Such Player Found&r")
                } else {
                    val giver = when (player) {
                        is ConsoleCommandSender -> null
                        is Player -> findPlayerByUUID(player.getUniqueId())
                        else -> null
                    }
                    val record = playerRecord.performAction(ActionType.Kick, combinedReason, giver)
                    output.appendLine(
                        formatAction(
                            ActionType.Kick,
                            playerRecord.mostRecentName(),
                            giver?.mostRecentName() ?: "CONSOLE",
                            durationLess = true
                        )
                    )
                    Bukkit.getPlayer(playerRecord.uuid)?.kickPlayer(record.format(fmt).colorize())
                }
                output.toString().colorize().broadcastToPermission(permission)
            }
        }
    }
}
