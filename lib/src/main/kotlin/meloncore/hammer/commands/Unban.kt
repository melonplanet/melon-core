package net.melonpla.core.hammer.commands
import com.github.ajalt.clikt.core.*
import com.github.ajalt.clikt.parameters.arguments.*
import com.github.ajalt.clikt.parameters.options.*
import net.melonpla.core.hammer.format.formatAction
import net.melonpla.core.hammer.player.*
import net.melonpla.core.util.*
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

class HammerUnban(val player: CommandSender, val db: Database) : CliktCommand(
    help = "Unbans a player",
    name = "unban"
) {
    val playerName by argument(help = "Player name or UUID")
    val reason by argument(help = "Reason for ban").multiple()
    val permission = "meloncore.hammer.ban"
    override fun run() {
        val combinedReason = reason.joinToString(separator = " ")
        if (!player.hasPermission(permission) && !(player is ConsoleCommandSender)) {
            player.sendMessage("You do not have permission to run this command")
        } else {
            transaction(db) {
                val output = StringBuilder()
                val uuid = parseUUID(playerName)
                val playerRecord = uuid?.let { findPlayerByUUID(it) } ?: findPlayerByName(
                    playerName
                )
                if (playerRecord == null) {
                    output.appendLine("&4No Such Player Found&r")
                } else {
                    val giver = when (player) {
                        is ConsoleCommandSender -> null
                        is Player -> findPlayerByUUID(player.getUniqueId())
                        else -> null
                    }
                    playerRecord.performAction(ActionType.Unban, combinedReason, giver)
                    output.appendLine(
                        formatAction(
                            ActionType.Unban,
                            playerRecord.mostRecentName(),
                            giver?.mostRecentName() ?: "CONSOLE",
                            combinedReason,
                            durationLess = true
                        )
                    )
                }
                output.toString().colorize().broadcastToPermission(permission)
            }
        }
    }
}
