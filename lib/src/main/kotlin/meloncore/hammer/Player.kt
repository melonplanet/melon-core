package net.melonpla.core.hammer.player

import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.*
import org.stringtemplate.v4.*
import java.time.Duration
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.UUID

enum class ActionType(val status: String) {
    Warning("WARN"),
    Kick("KICK"),
    Ban("BAN"),
    Unban("UNBAN")
}

object PlayerNamesRecords : IntIdTable() {
    var player = reference("playerRecord", PlayerRecords)
    var name = varchar("name", 256)
    var lastSeen = long("lastSeen")
}

public class PlayerNamesRecord(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, PlayerNamesRecord>(PlayerNamesRecords)
    var player by PlayerNamesRecords.player
    var name by PlayerNamesRecords.name
    var lastSeen by PlayerNamesRecords.lastSeen
}

object PlayerIPRecords : IntIdTable() {
    var player = reference("playerRecord", PlayerRecords)
    var ip = varchar("ip", 64)
    var lastSeen = long("lastSeen")
}

public class PlayerIPRecord(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, PlayerIPRecord>(PlayerIPRecords)
    var player by PlayerIPRecords.player
    var ip by PlayerIPRecords.ip
    var lastSeen by PlayerIPRecords.lastSeen
}

object PlayerActionRecords : IntIdTable() {
    var player = reference("playerRecord", PlayerRecords)
    var type = enumerationByName("actionType", 64, ActionType::class)
    var time = long("time")
    var expires = long("expires").nullable()
    var reason = varchar("reason", 256)
    var provider = long("giver")
}

public class PlayerActionRecord(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, PlayerActionRecord>(PlayerActionRecords)
    var player by PlayerActionRecords.player
    var type by PlayerActionRecords.type
    var time by PlayerActionRecords.time
    var expires by PlayerActionRecords.expires
    var reason by PlayerActionRecords.reason
    var provider by PlayerActionRecords.provider
    val dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mma z").withZone(
        ZoneId.systemDefault()
    )

    fun format(fmt: String, alt: String? = null): String {
        val st = ST(fmt)
        val playerRecord = PlayerRecord.findById(player.value)!!
        val madeDate = LocalDateTime
            .ofInstant(Instant.ofEpochMilli(time * 1000), ZoneId.systemDefault())
            .format(dateFormatter)
        val expiresDate = expires?.let {
            LocalDateTime
                .ofInstant(Instant.ofEpochMilli(it * 1000), ZoneId.systemDefault())
                .format(dateFormatter)
        } ?: "Never"
        val giverName = if (provider != -1L) {
            PlayerRecord.findById((provider).toInt())!!.mostRecentName()
        } else {
            "CONSOLE"
        }
        val currentTime = LocalDateTime.now()
            .format(dateFormatter)
        st.add("name", playerRecord.mostRecentName())
        st.add("date", madeDate)
        st.add("expires", expiresDate)
        st.add("reason", reason)
        st.add("giver", giverName)
        st.add("currentTime", currentTime)
        if (alt != null) { st.add(alt, "<alt>") }
        return st.render()
    }

    fun formatLog(fmt: String): String {
        val st = ST(fmt)
        val playerRecord = PlayerRecord.findById(player.value)!!
        val madeDate = LocalDateTime
            .ofInstant(Instant.ofEpochMilli(time * 1000), ZoneId.systemDefault())
            .format(dateFormatter)
        val expiresDate = expires?.let {
            LocalDateTime
                .ofInstant(Instant.ofEpochMilli(it * 1000), ZoneId.systemDefault())
                .format(dateFormatter)
        } ?: "Never"
        val giverName = if (provider != -1L) {
            PlayerRecord.findById((provider).toInt())!!.mostRecentName()
        } else {
            "CONSOLE"
        }
        val currentTime = LocalDateTime.now()
            .format(dateFormatter)

        val typeString = when (type) {
            ActionType.Unban -> "&aUNBAN&r"
            ActionType.Warning -> "&eWARN&r"
            ActionType.Kick -> "&6KICK&r"
            ActionType.Ban -> "&4BAN&r"
        }

        st.add("name", playerRecord.mostRecentName())
        st.add("date", madeDate)
        st.add("expires", expiresDate)
        st.add("reason", reason)
        st.add("giver", giverName)
        st.add("currentTime", currentTime)
        st.add("type", typeString)
        return st.render()
    }

    fun expired(): Boolean {
        return expires?.let { it <= (System.currentTimeMillis() / 1000) } ?: false
    }

    fun expires(): Boolean {
        return expires != null
    }
}

object PlayerRecords : IntIdTable() {
    var uuid = uuid("uuid")
    var lastSeen = long("lastSeen")
}

public class PlayerRecord(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, PlayerRecord>(PlayerRecords)
    var uuid by PlayerRecords.uuid
    var lastSeen by PlayerRecords.lastSeen
    val nameRecords by PlayerNamesRecord referrersOn PlayerNamesRecords.player
    val ipRecords by PlayerIPRecord referrersOn PlayerIPRecords.player
    val actionRecords by PlayerActionRecord referrersOn PlayerActionRecords.player

    fun updateName(newName: String) {
        val time = System.currentTimeMillis() / 1000
        val hit = nameRecords.find { it.name == newName }
        if (hit == null) {
            val id = this.id.value
            PlayerNamesRecords.insertAndGetId {
                it[player] = id
                it[name] = newName
                it[lastSeen] = time
            }
        } else {
            hit.lastSeen = time
        }
    }

    fun updateIp(newIp: String) {
        val time = System.currentTimeMillis() / 1000
        val hit = ipRecords.find { it.ip == newIp }
        if (hit == null) {
            val id = this.id.value
            PlayerIPRecords.insertAndGetId {
                it[player] = id
                it[ip] = newIp
                it[lastSeen] = time
            }
        } else {
            hit.lastSeen = time
        }
    }

    fun mostRecentName(): String {
        // As a database invariant, this must be non-null
        return this.nameRecords.maxByOrNull { it.lastSeen }!!.name
    }

    fun activeBan(): PlayerActionRecord? {
        val currentTime = System.currentTimeMillis() / 1000
        val canidates = this.actionRecords
            .sortedByDescending { it.time }
            .filter { it.type == ActionType.Ban }
            .filter { it.expires?.let { it >= currentTime } ?: true }
        if (canidates.count() < 1) {
            return null
        } else {
            val mostRecentBan = canidates.first()
            val unbans = this.actionRecords
                .sortedByDescending { it.time }
                .filter { it.type == ActionType.Unban }
                .filter { it.time >= mostRecentBan.time }
            if (unbans.count() > 0) {
                return null
            } else {
                return mostRecentBan
            }
        }
    }

    fun performAction(
        actionType: ActionType,
        actionReason: String,
        actionGiver: PlayerRecord?,
        expiryTime: Duration? = null
    ): PlayerActionRecord {
        val currentTime = System.currentTimeMillis() / 1000
        val expiry = expiryTime?.let { (it.toMillis() / 1000) + currentTime }
        val id = this.id.value
        val giverId = actionGiver?.let { it.id.value } ?: -1
        val recordId = PlayerActionRecords.insertAndGetId {
            it[player] = id
            it[type] = actionType
            it[time] = currentTime
            it[expires] = expiry
            it[reason] = actionReason
            it[provider] = giverId.toLong()
        }

        return PlayerActionRecord.findById(recordId)!!
    }
}

fun findOrCreatePlayer(playerUuid: UUID): Pair<PlayerRecord, Boolean> {
    val time = System.currentTimeMillis() / 1000
    var playerCanidates = PlayerRecord.find { PlayerRecords.uuid eq playerUuid }
    if (playerCanidates.count() < 1) {
        return Pair(
            PlayerRecord.new {
                uuid = playerUuid
                lastSeen = time
            },
            true
        )
    } else {
        return Pair(playerCanidates.first(), false)
    }
}

fun findPlayerByUUID(playerUuid: UUID): PlayerRecord? {
    val playerCanidates = PlayerRecord.find { PlayerRecords.uuid eq playerUuid }
    if (playerCanidates.count() < 1) {
        return null
    } else {
        return playerCanidates.first()
    }
}

fun findPlayerByName(playerName: String): PlayerRecord? {
    val nameCanidates = PlayerNamesRecord.find { PlayerNamesRecords.name eq playerName }
        .sortedByDescending { it.lastSeen }
    if (nameCanidates.count() < 1) {
        return null
    } else {
        return PlayerRecord.findById(nameCanidates.first().player.value)
    }
}

fun possibleAlts(playerRecord: PlayerRecord): List<PlayerRecord> {
    val ips = playerRecord.ipRecords.map { it.ip }
    val ipRecordCanidates = PlayerIPRecord.find { PlayerIPRecords.ip inList ips }.map { it.player.value }
    return PlayerRecord.find {
        PlayerRecords.id inList ipRecordCanidates
    }.filter { it.id != playerRecord.id }.toList()
}

fun playersByIp(playerIp: String): List<PlayerRecord> {
    val ipRecords = PlayerIPRecord.find { PlayerIPRecords.ip eq playerIp }
    return ipRecords.map { PlayerRecord.findById(it.id.value) }.filterNotNull()
}
