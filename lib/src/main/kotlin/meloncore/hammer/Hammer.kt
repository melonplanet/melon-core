package net.melonpla.core.hammer

import com.github.ajalt.clikt.core.*
import net.melonpla.core.command.MelonCommand
import net.melonpla.core.hammer.commands.*
import net.melonpla.core.hammer.player.*
import net.melonpla.core.util.*
import org.bukkit.command.CommandSender
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerLoginEvent
import org.bukkit.plugin.java.JavaPlugin
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.LoggerFactory

public class Hammer : MelonCommand, Listener {
    val config: ConfigurationSection
    val plugin: JavaPlugin
    val db: Database
    val dbName: String
    val banMessage: String
    val kickMessage: String
    val warnMessage: String
    constructor (config: ConfigurationSection, plugin: JavaPlugin) :
        super(
            "hammer",
            "Deploys the banhammer",
            "/hammer (ban|unban|kick|warn|history|help) <target> [options]",
            "meloncore.hammer",
            LoggerFactory.getLogger("MelonHammer")
        ) {
            this.config = config
            this.plugin = plugin

            dbName = config.getString("database.h2") ?: "hammer"

            val dbPath = "jdbc:h2:" + plugin.getDataFolder().getAbsolutePath() + "/" + dbName
            log.info("Opening db connection at: " + dbPath)

            db = Database.connect(dbPath, driver = "org.h2.Driver", user = "root", password = "")
            log.info("Db connection opened")
            transaction(db) {
                log.info("Creating schema")
                SchemaUtils.create(
                    PlayerRecords,
                    PlayerNamesRecords,
                    PlayerIPRecords,
                    PlayerActionRecords
                )
            }

            banMessage = config.getStringList("banMessage").joinToString(separator = "&r\n")
            kickMessage = config.getStringList("kickMessage").joinToString(separator = "&r\n")
            warnMessage = config.getStringList("warnMessage").joinToString(separator = "&r\n")
        }

    override fun genCommand(sender: CommandSender, alias: String, args: Array<out String>): CliktCommand {
        return HammerCommand(sender)
            .subcommands(
                HammerHistory(sender, db),
                HammerBan(sender, db, banMessage),
                HammerKick(sender, db, kickMessage),
                HammerUnban(sender, db),
                HammerWarn(sender, db, warnMessage),
                HammerChat(sender)
            )
    }

    @EventHandler
    public fun onPlayerLogin(event: PlayerLoginEvent) {
        val thePlayer = event.getPlayer()
        val playerUuid = thePlayer.getUniqueId()
        // Open a database transaction
        transaction(db) {
            // find the player
            val playerName = thePlayer.getPlayerListName()
            val playerIp = event.getAddress().toString()
            val (playerRecord, created) = findOrCreatePlayer(playerUuid)
            if (created) {
                log.info("Player " + playerName + " (" + playerUuid + ") not seen before, creating")
            } else {
                log.info("Player " + playerName + " has been seen before")
            }
            // Update the name and ip database
            playerRecord.updateName(playerName)
            playerRecord.updateIp(playerIp)

            log.info(
                "Known player names: " + playerRecord.nameRecords.map { it.name.toString() }.joinToString()
            )
            log.info("Known player ips: " + playerRecord.ipRecords.map { it.ip }.joinToString())
            val playersString = playersByIp(playerIp).map { it.mostRecentName() }.joinToString()
            log.info(
                "Known players with this ip: $playersString"
            )

            // Check for a ban by player
            val activeBan = playerRecord.activeBan()
            if (activeBan != null) {
                val message = activeBan.format(banMessage).colorize()
                event.disallow(PlayerLoginEvent.Result.KICK_BANNED, message)
            }
        }
    }
}

class HammerCommand(val player: CommandSender) : CliktCommand() {
    override fun run() = Unit
}
