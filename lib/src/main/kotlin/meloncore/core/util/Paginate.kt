package net.melonpla.core.util

import org.stringtemplate.v4.ST

fun String.paginate(headerFmt: String, page: Int, linesPerPage: Int = 9): String {
    // Colorize the input first, then split it by lines
    val lines = this.colorize().lines()
    val totalPages = lines.count() / linesPerPage + 1
    val headerST = ST(headerFmt)
    headerST.add("page", page.toString())
    headerST.add("total", totalPages.toString())
    val header = headerST.render().colorize()
    val output = StringBuilder()
    output.appendLine(header)
    for (line in lines.drop(linesPerPage * (page - 1)).take(linesPerPage)) {
        output.appendLine(line)
    }

    return output.toString()
}

fun String.paginateWithBreaks(
    headerFmt: String,
    page: Int,
    linesPerPage: Int = 9,
    breakMarker: String = "***"
): String {
    // Split on the breakMarker, and break each section into its pages
    val pages = this
        .split(breakMarker)
        .map { it.lines() }
        .map { it.filter { !it.trim().replace("&r", "").isEmpty() } }
        .map { it.chunked(linesPerPage) }
        .map { it.map { it.joinToString(separator = "\n") } }
        .flatten()

    val total = pages.count()
    val headerST = ST(headerFmt)
    headerST.add("page", page)
    headerST.add("total", total)
    val output = StringBuilder()
    output.appendLine(headerST.render())
    pages.getOrNull(page - 1)?.let { output.append(it) }

    return output.toString().colorize()
}
