package net.melonpla.core.util

import org.bukkit.Bukkit
import java.util.UUID

fun parseUUID(input: String): UUID? {
    try {
        val uuid = UUID.fromString(input)
        return uuid
    } catch (e: IllegalArgumentException) {
        return null
    }
}

public fun UUID.playerName(): String? {
    val player = Bukkit.getServer().getOfflinePlayer(this)
    if (player.hasPlayedBefore()) {
        return player.getName()
    } else {
        return null
    }
}
