package net.melonpla.core.util

import org.bukkit.Bukkit

fun String.broadcastToPermission(permission: String) {
    // First broadcast to console
    Bukkit.getServer().getConsoleSender().sendMessage(this)
    // Send message to every player having the permission
    for (player in Bukkit.getOnlinePlayers()) {
        if (player.hasPermission(permission)) {
            player.sendMessage(this)
        }
    }
}
