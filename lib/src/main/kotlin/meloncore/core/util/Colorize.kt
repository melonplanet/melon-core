package net.melonpla.core.util

import org.bukkit.ChatColor

fun String.colorize() = ChatColor.translateAlternateColorCodes('&', this)
