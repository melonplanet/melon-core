package net.melonpla.core.command

import com.github.ajalt.clikt.core.*
import com.github.ajalt.clikt.output.CliktConsole
import com.github.ajalt.clikt.parameters.arguments.*
import com.github.ajalt.clikt.parameters.groups.*
import com.github.ajalt.clikt.parameters.options.*
import net.melonpla.core.util.*
import org.bukkit.command.CommandSender
import org.bukkit.command.defaults.BukkitCommand
import org.bukkit.entity.Player
import org.slf4j.Logger

public abstract class MelonCommand(
    name: String,
    description: String,
    usageMessage: String,
    mPermission: String,
    val log: Logger,
    mPermissionMessage: String = "&4You do not have permission to use this command&r",
    val onlyPlayer: Boolean = false,
    val requiresPermission: Boolean = true,
) : BukkitCommand(name, description, usageMessage, emptyList()) {
    init {
        this.permission = mPermission
        this.permissionMessage = mPermissionMessage
    }

    override fun execute(sender: CommandSender, alias: String, args: Array<out String>): Boolean {
        if ((sender !is Player) && onlyPlayer) {
            sender.sendMessage("&4This command must be run by a player.&r".colorize())
            return false
        } else if (!sender.hasPermission(this.permission!!) && requiresPermission) {
            sender.sendMessage(permissionMessage!!.colorize())
            return false
        } else {
            val con = StringConsole()
            val cmd = genCommand(sender, alias, args).context { console = con }

            try {
                cmd.parse(args.toList())
            } catch (t: PrintHelpMessage) {
                var message =
                    ("&4" + t.command.getFormattedHelp() + "&r").colorize()
                sender.sendMessage(message)
            } catch (t: MissingArgument) {
                val name = t.argument!!.name
                val help = t.argument!!.argumentHelp
                var message =
                    ("&4Missing Argument&r: " + name + " - " + help).colorize()
                sender.sendMessage(message)
            } catch (t: NoSuchSubcommand) {
                var message =
                    ("&4" + cmd.getFormattedHelp() + "&r").colorize()
                sender.sendMessage(message)
            } catch (t: Throwable) {
                log.info("Failed to run command: " + t.toString())
            }

            return true
        }
    }

    abstract fun genCommand(sender: CommandSender, alias: String, args: Array<out String>): CliktCommand
}

class StringConsole : CliktConsole {
    override val lineSeparator: String = "\n"
    var output: String = ""
    override fun print(text: String, error: Boolean) {
        if (error) {
            output = output + "&4" + text + "&r"
        } else {
            output = output + text
        }
    }

    override fun promptForLine(prompt: String, hideInput: Boolean): String? = null

    fun render(): String {
        return output.colorize()
    }
}
