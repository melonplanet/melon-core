package net.melonpla.core.chat

import net.melonpla.core.util.*
import net.milkbowl.vault.chat.Chat
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.AsyncPlayerChatEvent
import org.bukkit.plugin.java.JavaPlugin
import org.jetbrains.exposed.sql.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.stringtemplate.v4.ST

public class Chat : Listener {
    val config: ConfigurationSection
    val plugin: JavaPlugin
    val log: Logger
    val formatTemplate: String
    val chat: Chat
    constructor (config: ConfigurationSection, plugin: JavaPlugin) {
        this.config = config
        this.plugin = plugin
        log = LoggerFactory.getLogger("MelonChat")
        formatTemplate = config.getString("format")!!
        val rsp = plugin.getServer().getServicesManager().getRegistration(Chat::class.java)
        chat = rsp!!.getProvider()
        log.info("Using " + chat.getName() + " as chat provider.")
    }

    @EventHandler
    public fun onPlayerChat(event: AsyncPlayerChatEvent) {
        if (event.getPlayer().hasPermission("meloncore.chat.colors")) {
            event.setMessage(event.getMessage().colorize())
        }

        val prefix = chat.getPlayerPrefix(event.getPlayer())
        var m = ST(formatTemplate)
        m.add("user", "%s")
        m.add("message", "%s")
        m.add("prefix", prefix)

        event.setFormat(m.render().colorize())
    }
}
