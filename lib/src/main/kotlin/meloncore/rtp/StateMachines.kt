package net.melonpla.core.rtp.state

import net.melonpla.core.util.*
import org.bukkit.Chunk
import org.bukkit.ChunkSnapshot
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.data.BlockData
import org.bukkit.block.data.type.Leaves
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitRunnable
import org.slf4j.Logger
import java.util.concurrent.CompletableFuture
import kotlin.random.Random

data class RTPData(
    val player: Player,
    val plugin: JavaPlugin,
    val log: Logger,
    var retries: Int,
    val xRange: Pair<Int, Int>,
    val zRange: Pair<Int, Int>,
)

sealed class MainTaskState(val rdata: RTPData) : BukkitRunnable() {
    abstract fun advance(): MainTaskState

    // End of execution
    class Completed(rdata: RTPData) : MainTaskState(rdata) {
        override fun advance() = MainTaskState.Completed(rdata)
    }

    // About to locate and get the chunk future for a chunk to check
    class StartOfRound(rdata: RTPData) : MainTaskState(rdata) {
        override fun advance(): MainTaskState {
            // Complete if we are out of retries
            if (rdata.retries <= 0) {
                rdata.log.info(
                    "&7[MelonRTP]&r Failed to find a safe location.".colorize()
                )
                return Completed(rdata)
            } else {
                rdata.player.sendMessage(
                    "&7[MelonRTP]&r Attempting to teleport &8(Attempt ${rdata.retries})".colorize()
                )
                // Subtract a retry for the next go around
                rdata.retries = rdata.retries - 1
                // Select a random chunk
                val x = Random.nextInt(rdata.xRange.first, rdata.xRange.second) / 16
                val z = Random.nextInt(rdata.zRange.first, rdata.zRange.second) / 16
                // Get the future for it
                val chunkFuture = rdata.player.getLocation().getWorld().getChunkAtAsync(x, z)
                // Advance to the next state
                return WaitingOnChunk(rdata, chunkFuture)
            }
        }
    }

    // Waiting on a chunk future
    class WaitingOnChunk(rdata: RTPData, val chunk: CompletableFuture<Chunk>) : MainTaskState(rdata) {
        override fun advance(): MainTaskState {
            // Check for chunk completion
            if (chunk.isDone()) {
                // get snapshot and pass to scanner
                val snap = chunk.get().getChunkSnapshot(true, false, false)
                val location = scanChunk(snap, rdata.plugin)
                // Advance to the next state
                return WaitingOnScan(rdata, location)
            } else {
                // Stay waiting (for someone)
                return WaitingOnChunk(rdata, chunk)
            }
        }
    }

    // Waiting on a chunk scan future
    class WaitingOnScan(
        rdata: RTPData,
        val location: CompletableFuture<Triple<Double, Double, Double>?>
    ) : MainTaskState(rdata) {
        override fun advance(): MainTaskState {
            // Check for scan completion
            if (location.isDone()) {
                // Unpack the location
                val loc = location.get()
                // Check for completion
                if (loc == null) {
                    // Send back to start
                    return StartOfRound(rdata)
                } else {
                    // Log it
                    val (x, y, z) = loc
                    val name = rdata.player.getPlayerListName()
                    rdata.log.info(
                        "&7Attempt ${rdata.retries}($name):&r ($x,$y,$z) is safe, teleporting."
                            .colorize()
                    )
                    rdata.player.sendMessage(
                        "&7[MelonRTP]&r Found a safe location, teleporting.".colorize()
                    )
                    val block = Location(rdata.player.getLocation().getWorld(), x, y, z)
                    val dest = block.add(
                        if (block.getX() > 0) 0.5 else -0.5,
                        1.0,
                        if (block.getZ() > 0) 0.5 else -0.5
                    )
                    rdata.player.teleportAsync(dest)
                    return Completed(rdata)
                }
            } else {
                // Stay waiting
                return WaitingOnScan(rdata, location)
            }
        }
    }

    override fun run() {
        // Attempt to advance the state machine
        val next = this.advance()
        when (next) {
            // If the state machine completed, we are done here
            is Completed -> return
            // Otherwise, try to advance again on the next tick
            else -> next.runTaskLater(rdata.plugin, 1)
        }
    }
}

fun scanChunk(chunk: ChunkSnapshot, plugin: JavaPlugin): CompletableFuture<Triple<Double, Double, Double>?> {
    val future: CompletableFuture<Triple<Double, Double, Double>?> = CompletableFuture()
    val task = object : BukkitRunnable() {
        public override fun run() {
            val coords = (0..15)
                .map { x -> (0..15).map { z -> Pair(x, z) } }
                .flatten()
                .map { (x, z) -> Triple(x, chunk.getHighestBlockYAt(x, z), z) }
                .filter { (x, y, z) -> !chunk.getBlockData(x, y, z).isBannedRTP() }
                .shuffled()
                .firstOrNull()
                ?.let {
                    (cx, y, cz) ->
                    val x = cx + chunk.getX() * 16
                    val z = cz + chunk.getZ() * 16
                    Triple(x.toDouble(), y.toDouble(), z.toDouble())
                }
            future.complete(coords)
        }
    }
    task.runTaskAsynchronously(plugin)
    return future
}

fun BlockData.isBannedRTP(): Boolean {
    val material = this.getMaterial()
    return material.isEmpty() || this is Leaves || material == Material.WATER || material == Material.LAVA
}
