package net.melonpla.core.rtp

import net.melonpla.core.rtp.state.*
import net.melonpla.core.util.*
import org.bukkit.command.CommandSender
import org.bukkit.command.defaults.BukkitCommand
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import org.slf4j.Logger
import org.slf4j.LoggerFactory

public class RTP(val config: ConfigurationSection, val plugin: JavaPlugin) : BukkitCommand("rtp") {
    val retries: Int = config.getInt("retries")
    val log: Logger = LoggerFactory.getLogger("MelonRTP")
    override fun execute(sender: CommandSender, alias: String, args: Array<out String>): Boolean {
        if (sender !is Player) {
            sender.sendMessage("&4This command can only be used by a player&r".colorize())
            return false
        } else if (!sender.hasPermission("meloncore.rtp")) {
            sender.sendMessage("&4You do not have permission to use this command&r".colorize())
        }
        val world = sender.getLocation().getWorld().getName()
        val section = config.getConfigurationSection(world)
        if ((section == null) || !(section.getBoolean("allowed", false))) {
            sender.sendMessage(
                "&7[MelonRTP]&r RTP is not allowed in this world"
                    .colorize()
            )
        } else {
            sender.sendMessage(
                "&7[MelonRTP]&r Finding a location to teleport to (this may take some time)"
                    .colorize()
            )
            val xMin = section.getInt("x-min")
            val zMin = section.getInt("z-min")
            val xMax = section.getInt("x-max")
            val zMax = section.getInt("z-max")

            val data = RTPData(
                sender,
                plugin,
                log,
                retries,
                Pair(xMin, xMax),
                Pair(zMin, zMax)
            )

            MainTaskState.StartOfRound(data).runTaskLater(plugin, 1)
        }
        return true
    }
}
