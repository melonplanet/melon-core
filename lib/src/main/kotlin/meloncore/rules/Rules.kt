package net.melonpla.core.rules

import net.melonpla.core.util.*
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.command.defaults.BukkitCommand
import org.bukkit.configuration.ConfigurationSection

public class Rules(val config: ConfigurationSection) : BukkitCommand("rules") {
    val message: String = config.getStringList("rules").joinToString("&r\n")
    init {
        this.description = "Display the rules"
        this.usageMessage = "/rules <page>"
        this.setPermission("meloncore.rules")
    }

    override fun execute(sender: CommandSender, alias: String, args: Array<out String>): Boolean {
        if (!sender.hasPermission(this.getPermission()!!) && (sender !is ConsoleCommandSender)) {
            sender.sendMessage("You do not have permission to run this command")
            return true
        } else if (args.size == 0) {
            sender.sendMessage(
                message.paginateWithBreaks(
                    "&6--- Rules Page <page> of <total> (&7/rules [page]&6) ---&r",
                    1
                )
            )
            return true
        } else if (args.size == 1) {
            val page = args[0].toIntOrNull()
            if (page == null || page < 1) {
                sender.sendMessage("&cUsage:&7 /rules <page>&r".colorize())
                return false
            } else {
                sender.sendMessage(
                    message.paginateWithBreaks(
                        "&6--- Rules Page <page> of <total> (&7/rules [page]&6) ---&r",
                        page
                    )
                )
                return true
            }
        }
        sender.sendMessage("&cUsage:&7 /rules <page>&r".colorize())
        return false
    }
}
